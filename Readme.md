# Keywords extracter
## Description
Extraction of keywords from a text of interest is a common task in text processing and is useful, for example, to automate annotating of various kinds of texts.

Algorithm was taken from: 
* Rose S., Engel D., Cramer N. and Cowley W. (2010), Automatic keyword
extraction from individual documents. Text Mining: Applications and Theory, John
Wiley & Sons, Ltd

[Whole manuscript](http://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&ved=0CEUQFjAB&url=http%3A%2F%2Fmedia.wiley.com%2Fproduct_data%2Fexcerpt%2F22%2F04707498%2F0470749822.pdf&ei=u1abUJjnC4z0iwKKm4D4Cg&usg=AFQjCNFoMLxWUWyVmHGIRvB2rhc-5S-TdA&cad=rja)

## Requirements
NLTK library: http://nltk.org/install.html
NLTK installation: http://www.nltk.org/install.html

* Install Setuptools: http://pypi.python.org/pypi/setuptools
* Install Pip: ```run sudo easy_install pip```
* Install Numpy (optional): run ```sudo pip install -U numpy```
* Install NLTK: run ```sudo pip install -U nltk```
* Test installation: run ```python``` then type ```import nltk```

You may want to read their book also: http://nltk.org/book/

##How to use

```
python keywords_extractor.py -t <filename.txt> 
```
In <filename.txt> you must provide your text of interest.

## Example

Command string:
```
python kw.py -t test.txt
```

Output:
```
The list of key words:
nonstrict inequations
linear diophantine equations
systems
natural numbers criteria
minimal supporting set
strict inequations
linear constraints
minimal generating
minimal set
upper bounds

```
## Contact

Any feedbacks are welcome!
Ilya Y. Zhbannikov, email: ilyaz@uidaho.edu, zhba3458@vandals.uidaho.edu
# -*- coding:utf8 -*-
#!/usr/bin/env python

__author__="Ilya Zhbannikov"
__date__ ="$01.11.2012 03:08:31$"
__modified__="$28.10.2014 05:34:54$"

import sys
import getopt
import nltk
import fileinput
import argparse

text_to_parse = ""
keyword_candidates = []
cooccurrence_matrix = []
words = dict()

def make_keyword_candidates(text):
    global keyword_candidates
    pattern = "NP:{<VB|VBN|JJ|JJR>*<NN|NNS|NNP>*}"
    NPChunker = nltk.RegexpParser(pattern)
    sentences = text.split('. ')
    for sentence in sentences :
        tokens = nltk.word_tokenize(sentence.strip())
        tags = nltk.pos_tag(tokens)
        result = NPChunker.parse(tags)
        #print result
        for n in result:
          if isinstance(n, nltk.Tree):
            if n.label() == 'NP':
                kcand = ""
                for item in n.leaves() :
                    kcand+= item[0] + ' ' if len(item[0]) >= 3 else ''
                #print kcand
                if len(kcand) > 3 :
                    keyword_candidates.append(kcand.strip())
			
    
    

def make_cooccurrence_matrix():
    global cooccurrence_matrix
    global keyword_candidates
    #print keyword_candidates
    global words
    
    for item in keyword_candidates :
        ii = item.split(' ')
        for i in ii :
            words[i] = 1

    for i in xrange(len(words)):
      cooccurrence_matrix.append([])
      for j in xrange(len(words)):
        cooccurrence_matrix[i].append(0)
    wkeys = list(words)
    for i in xrange(len(words)) : #Row
        for z in xrange(len(keyword_candidates)) :
            if wkeys[i] in [kc.strip() for kc in keyword_candidates[z].split(' ') ] :
                for j in xrange(len(words)) : # Column
                    if wkeys[j] in [kc.strip() for kc in keyword_candidates[z].split(' ') ] :
                        cooccurrence_matrix[i][j]+=1
                        

           

    

def make_keywords_list():
    global cooccurrence_matrix
    global keyword_candidates
    global words
    word_freqs = dict()
    word_deg = dict()
    score_array = dict()
    keywords = dict()
    
    wkeys = list(words)
    
    for i in xrange(len(cooccurrence_matrix)) :
        deg = 0.0
        for j in xrange(len(cooccurrence_matrix)) :
            deg += cooccurrence_matrix[i][j]
            if i == j :
                word_freqs[wkeys[i]] = cooccurrence_matrix[i][j]
        word_deg[wkeys[i]] = deg

    for i in xrange(len(cooccurrence_matrix)) :
        score_array[wkeys[i]] = word_deg[wkeys[i]]/word_freqs[wkeys[i]]

    for i in xrange(len(keyword_candidates)) :
        keywords[keyword_candidates[i]] = 0
    
    for i in xrange(len(score_array)) :
        for j in xrange(len(keyword_candidates)) :
            if keyword_candidates[j].rfind(wkeys[i]) != -1 :
                keywords[keyword_candidates[j]] += score_array[wkeys[i]]
                #print score

        

    #print sorted(keywords.items(), key=lambda x: x[1], reverse = True)
    keywords_keys = list( sorted(keywords.items(), key=lambda x: x[1], reverse = True))
    print "The list of key words:"
    i = 0
    for item in keywords_keys :
        print item[0]
        i+=1
        if i > len(keywords_keys)/3 +1 : break


def print_cooccurrence_matrix() :
   global cooccurrence_matrix
   print cooccurrence_matrix
        
def read_text_file(filename) :
  global text_to_parse
  try :
    with open(filename, 'r') as f:
     text_to_parse = f.read()
      
  except IOError:
    print "File " + filename + " not found"
    sys.exit(2)
        
def main(argv):
   """Parses command line arguments and runs keyword search"""
   # Parsing command line arguments:
   parser = argparse.ArgumentParser(prog='kw.py', usage='%(prog)s -t [--text] INFILE [options]') # argparse parser initialization
   # Adding arguments:
   parser.add_argument("-t", "--text", dest="infile", action="store", help="Input file with a text to parse.", required=True)
   parser.add_argument("-o", "--output", dest="outfile", action="store", help="Output file with keywords.")
   parser.add_argument("-v", "--verbose", help="Verbose output (results to a screen)", action="store_true")
   # Getting args from command line:
   args = parser.parse_args()
   
   # Reading input file:
   read_text_file(args.infile)
   # Keywords extracting:
   make_keyword_candidates(text_to_parse.lower())
   make_cooccurrence_matrix()
   make_keywords_list()
   
   sys.exit()
         
if __name__ == "__main__":
	
   main(sys.argv[1:])        